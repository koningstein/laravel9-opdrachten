<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Laravel Opdrachten

Deze repository is voor studenten die aan de slag willen met het leren van Laravel9. De opdrachten zijn gekoppeld aan de lessenserie die
geschreven is om stap voor stap het framework te leren. De eisen van de opdrachten staan dan ook in het document beschreven, met daarbij
het commando wat je kan uitvoeren als controle.

De levels van de lessenserie zijn:
<ul>
    <li>Level 0: Installatie & basis uitleg van Laravel</li>
    <li>Level 1: Een crud zonder relatie & simpele publieke pagina</li>
    <li>Level 2: Authenticatie, permissies en een crud met relatie</li>
    <li>Level 3: Geavanceerde publieke pagina’s</li>
    <li>Level 4: Testen met Phpunit / Pest / Dusk & Test driven development</li>
    <li>Level 5: Werken met API calls in combinatie met React / Vue</li>
</ul>

## De opdracht

Voor de opdracht zal je aan de slag gaan met een project tool. Dit is om bij te houden hoever een project staat, welke taken er zijn en
wie de taken moeten uitvoeren. De functionaliteit bestaat uit het volgende:

<ul>
<li>Studenten kunnen een project aanmaken</li>
<li>Bij een project kunnen allerlei taken horen</li>
<li>Een taak heeft een status, bijvoorbeeld: Todo, Doing, Testing, Verify, Done</li>
<li>Een taak kan verschillende labels hebben, zoals: front-end, backend, documentation, bug, feature</li>
</ul>

En ja, het ziet er misschien lastig uit, maar de opdrachten zullen steeds een klein stapje zijn. De opdrachten zullen ook heel precies zijn,
er zijn namelijk automatische testen beschikbaar waar op alle details wordt gelet.

## Contact
Wil je ook aan de slag met deze opdrachten en heb je hiervoor de lessenserie met opdracht beschrijvingen nodig. Neem dan contact op met mij via m.koningstein@tcrmbo.nl 
